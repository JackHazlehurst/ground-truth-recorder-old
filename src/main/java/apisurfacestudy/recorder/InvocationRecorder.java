package apisurfacestudy.recorder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Manages the invocation recording process.
 * @author jens dietrich
 */
public class InvocationRecorder {

    private static Map<TestRecord, Integer> INVOCATION_COUNTER = new ConcurrentHashMap<>();
    private static boolean TRACK_ALL = false;
    private static boolean RECORD_AS_TEST = false;
    private static TestRecord TEST_RECORD = TestRecord.JUNIT_SETUP;
    private static Set<String> PACKAGE_PREFIXES = null;
    public static String PACKAGE_LIST_SYSTEM_PROPERTY = "trackpackages";
    public static String INVOCATIONS_FILE_NAME = "tracked-invocations";
    public static String INVOCATIONS_FILE_EXTENSION = ".csv";

    private static String CSV_SEP = "\t";

    static {
        String packagePrefixList = System.getProperty(PACKAGE_LIST_SYSTEM_PROPERTY);
        if (packagePrefixList==null) {
            throw new IllegalStateException("package prefix definition system property must be set " +
                    "(multiple names must be separated by comma). Set it to '*' to track all packages. " +
                    "The property name is: " + PACKAGE_LIST_SYSTEM_PROPERTY);
        }
        // '*' indicates all packages should be tracked
        else if ("*".equals(packagePrefixList)) {
            TRACK_ALL = true;
        }
        else {
            PACKAGE_PREFIXES = Stream.of(packagePrefixList.split(",")).map(s -> s.trim()).collect(Collectors.toSet());
        }

        // Adding shutdown hook to write tracked invocations to file
        Runtime.getRuntime().addShutdownHook(
            new Thread("invocation-recorder-shutdown-hook") {
                @Override
                public void run() {
                    super.run();
                    File invocationsFile = new File(INVOCATIONS_FILE_NAME + "-" + System.currentTimeMillis() + INVOCATIONS_FILE_EXTENSION);
                    try (PrintWriter pw = new PrintWriter(new FileWriter(invocationsFile))) {
                        String headerLine = toLine("testAnnotation", "testClass", "testMethod", "type", "class", "name", "descriptor", "count");
                        pw.println(headerLine);

                        INVOCATION_COUNTER.keySet().stream()
                                .sorted()
                                .map(InvocationRecorder::toDataLine)
                                .forEach(line -> pw.println(line));

                    } catch (IOException x) {
                        System.err.println(("error writing invocations to file : " + invocationsFile.getAbsolutePath()));
                        x.printStackTrace();
                    }
                    System.out.println("invocation-recorder: recorded invocations written to " + invocationsFile.getAbsolutePath());
                }
            }
        );
    }

    public static boolean shouldTrack(String className) {
        if (TRACK_ALL) {
            return true;
        }

        for (String pck:PACKAGE_PREFIXES) {
            if (className.startsWith(pck)) return true;
        }
        return false;
    }

    public static void stopTestTracking() {
        RECORD_AS_TEST = false;
    }

    public static void recordTestInvocation(TestRecord.TestAnnotation annotation, String className, String methodName) {
        RECORD_AS_TEST = true;

        switch (annotation) {
            case BEFORE_ALL:
            case AFTER_ALL: {
                // Do not specifically attribute these invocations to a test, do it to the whole class
                TEST_RECORD = new TestRecord(annotation, className, "<class>", null);
                break;
            }
            case BEFORE_EACH: {
                // Reset the test being assigned, nulls will be replace when the test is known
                TEST_RECORD = new TestRecord(annotation, className, null, null);
                break;
            }
            case TEST: {
                TEST_RECORD = new TestRecord(annotation, className, methodName, null);
                retroactivelyAssignTestMethod();
                break;
            }
            case AFTER_EACH: {
                TEST_RECORD = new TestRecord(TEST_RECORD, annotation);
                break;
            }
        }
    }

    private static void retroactivelyAssignTestMethod() {
        INVOCATION_COUNTER.keySet()
                .parallelStream()
                .filter(k -> k.testMethod == null && k.annotation != TestRecord.TestAnnotation.JUNIT_SETUP)
                .forEach(k -> {
                    Integer v = INVOCATION_COUNTER.get(k);
                    INVOCATION_COUNTER.remove(k);
                    INVOCATION_COUNTER.put(new TestRecord(TEST_RECORD.testMethod, k), v);
                });
    }

    public static void recordFieldReadAccess(String className, String fieldName, String descriptor) {
        recordEvent("field-read", className, fieldName, descriptor);
    }

    public static void recordFieldWriteAccess(String className, String fieldName, String descriptor) {
        recordEvent("field-write", className, fieldName, descriptor);
    }

    public static void recordMethodInvocation(String className, String methodName, String descriptor) {
        recordEvent("invocation", className, methodName, descriptor);
    }

    public static void recordConstructorInvocation(String className, String descriptor) {
        recordEvent("allocation", className, "<init>", descriptor);
    }

    private static void recordEvent(String event, String className, String name, String descriptor) {
        String line = toLine(event, className, name, descriptor);
        TestRecord testRecord = RECORD_AS_TEST ? TEST_RECORD : TestRecord.JUNIT_SETUP;
        INVOCATION_COUNTER.compute(new TestRecord(testRecord, line), (k,v) -> v==null ? 1 : (v+1));
    }

    private static String toLine(String... values) {
        return Stream.of(values).collect(Collectors.joining(CSV_SEP));
    }

    private static String toDataLine(TestRecord testRecord) {
        if (testRecord == null) {
            return toLine(null, null, null, null, null);
        }
        String annotation = testRecord.annotation == null ? null : testRecord.annotation.toString();

        return toLine(annotation,
                testRecord.testClass,
                testRecord.testMethod,
                testRecord.value,
                INVOCATION_COUNTER.get(testRecord).toString());
    }
}
