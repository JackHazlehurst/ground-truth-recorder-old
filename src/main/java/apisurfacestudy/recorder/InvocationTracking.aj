package apisurfacestudy.recorder;

import org.aspectj.lang.*;
import org.aspectj.lang.reflect.*;
import org.aspectj.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Aspect to add invocation tracking.
 * @author jens dietrich
 */
public aspect InvocationTracking {
    // Two versions of each annotation to support junit 4 and 5
    private static final String BEFORE_ALL = "(execution(@org.junit.BeforeClass * *()) || execution(@org.junit.jupiter.api.BeforeAll * *())) && !within(apisurfacestudy..*)";
    private static final String AFTER_ALL = "(execution(@org.junit.AfterClass * *()) || execution(@org.junit.jupiter.api.AfterAll * *())) && !within(apisurfacestudy..*)";
    private static final String BEFORE_EACH = "(execution(@org.junit.Before * *()) || execution(@org.junit.jupiter.api.BeforeEach * *())) && !within(apisurfacestudy..*)";
    private static final String AFTER_EACH = "(execution(@org.junit.After * *()) || execution(@org.junit.jupiter.api.AfterEach * *())) && !within(apisurfacestudy..*)";
    private static final String TEST = "(execution(@org.junit.Test * *()) || execution(@org.junit.jupiter.api.Test * *())) && !within(apisurfacestudy..*)";

    @Before("call(* *.*(..)) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackInvocations(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();
        if (InvocationRecorder.shouldTrack(className)) {
            String methodName = method.getName();
            String descr = extractDescriptor(method);
            InvocationRecorder.recordMethodInvocation(className, methodName, descr);
        }
    }

    @Before("call(*.new(..)) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackAllocations(JoinPoint joinPoint) {
        ConstructorSignature methodSignature = (ConstructorSignature) joinPoint.getSignature();
        Constructor constructor = methodSignature.getConstructor();
        String className = constructor.getDeclaringClass().getName();
        if (InvocationRecorder.shouldTrack(className)) {
            String descr = extractDescriptor(constructor);
            InvocationRecorder.recordConstructorInvocation(className, descr);
        }
    }

    @Before("get(* *.*) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackLoad(JoinPoint joinPoint) {
        FieldSignature fieldSignature = (FieldSignature) joinPoint.getSignature();
        Field field = fieldSignature.getField();
        String className = field.getDeclaringClass().getName();
        if (InvocationRecorder.shouldTrack(className)) {
            InvocationRecorder.recordFieldReadAccess(className, field.getName(), field.getType().getName());
        }
    }

    @Before("set(* *.*) && !within(java..*) && !within(apisurfacestudy..*)")
    public void trackStore(JoinPoint joinPoint) {
        FieldSignature fieldSignature = (FieldSignature) joinPoint.getSignature();
        Field field = fieldSignature.getField();
        String className = field.getDeclaringClass().getName();
        if (InvocationRecorder.shouldTrack(className)) {
            InvocationRecorder.recordFieldWriteAccess(className, field.getName(), field.getType().getName());
        }
    }

    @Before(BEFORE_ALL)
    public void trackBeforeAllInvocation(JoinPoint joinPoint) {
        trackJunitInvocation(joinPoint, TestRecord.TestAnnotation.BEFORE_ALL);
    }

    @Before(AFTER_ALL)
    public void trackAfterAllInvocation(JoinPoint joinPoint) {
        trackJunitInvocation(joinPoint, TestRecord.TestAnnotation.AFTER_ALL);
    }

    @Before(BEFORE_EACH)
    public void trackBeforeEachInvocation(JoinPoint joinPoint) {
        trackJunitInvocation(joinPoint, TestRecord.TestAnnotation.BEFORE_EACH);
    }

    @Before(AFTER_EACH)
    public void trackAfterEachInvocation(JoinPoint joinPoint) {
        trackJunitInvocation(joinPoint, TestRecord.TestAnnotation.AFTER_EACH);
    }

    @Before(TEST)
    public void trackTestInvocation(JoinPoint joinPoint) {
        trackJunitInvocation(joinPoint, TestRecord.TestAnnotation.TEST);
    }

    @After(BEFORE_ALL)
    public void stopBeforeAllTracking(JoinPoint joinPoint) {
        InvocationRecorder.stopTestTracking();
    }

    @After(AFTER_ALL)
    public void stopAfterAllTracking(JoinPoint joinPoint) {
        InvocationRecorder.stopTestTracking();
    }

    @After(BEFORE_EACH)
    public void stopBeforeEachTracking(JoinPoint joinPoint) {
        InvocationRecorder.stopTestTracking();
    }

    @After(AFTER_EACH)
    public void stopAfterEachTracking(JoinPoint joinPoint) {
        InvocationRecorder.stopTestTracking();
    }

    @After(TEST)
    public void stopTestTracking(JoinPoint joinPoint) {
        InvocationRecorder.stopTestTracking();
    }

    private void trackJunitInvocation(JoinPoint joinPoint, TestRecord.TestAnnotation annotation) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        String className = method.getDeclaringClass().getName();

        if (InvocationRecorder.shouldTrack(className)) {
            String methodName = method.getName();
            InvocationRecorder.recordTestInvocation(annotation, className, methodName);
        }
    }

    private String extractDescriptor(Method method) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : method.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        sb.append(method.getReturnType().getName());
        return sb.toString();
    }

    private String extractDescriptor(Constructor constructor) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        boolean f = true;
        for (Class paramType : constructor.getParameterTypes()) {
            if (f) f = false;
            else sb.append(',');
            sb.append(paramType.getName());
        }
        sb.append(')');
        return sb.toString();
    }
}