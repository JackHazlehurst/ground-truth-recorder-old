package apisurfacestudy.recorder;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Merge results files within a folder.
 * @author jens dietrich
 */
public class MergeUtil {


    public static void main (String[] args) {

        if (args.length!=1) {
            throw new IllegalArgumentException("One parameters expected -- the input folder");
        }
        File dir = new File(args[0]);
        if (!dir.exists()) {
            throw new IllegalArgumentException("Input folder does not exist: " + dir.getAbsolutePath());
        }
        if (!dir.isDirectory()) {
            throw new IllegalArgumentException("Must be a folder: " + dir.getAbsolutePath());
        }

        // lines look like this:
        // invocation	org.apache.logging.log4j.util.PropertyFilePropertySource	$jacocoInit	()[Z	12
        // merge counts (last value)
        Map<String,Integer> records = new HashMap<>();
        File mergedFile = new File(dir,"tracked-invocations-all.csv");
        for (File file:dir.listFiles((d,n) -> n.startsWith("tracked-invocations-")
            && n.endsWith(".csv")
            && !n.equals(mergedFile.getName()))
        ) {
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                System.out.println("Merging " + file.getName());
                boolean f = true;
                String line = null;
                while ((line=br.readLine())!=null) {
                    if (f) f = false; // skip first line with column names
                    else {
                        String[] tokens = line.split("\t");
                        assert tokens.length == 5;
                        String inv = Stream.of(tokens[0],tokens[1],tokens[2],tokens[3]).collect(Collectors.joining("\t"));
                        Integer count = Integer.parseInt(tokens[4]);
                        records.compute(inv,(k,v) -> v==null?count:v+count);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (PrintWriter pw = new PrintWriter(new FileWriter(mergedFile))) {
            String headerLine = Stream.of("type","class","name","descriptor","count").collect(Collectors.joining("\t"));
            pw.println(headerLine);
            for (String k:records.keySet()) {
                pw.print(k);
                pw.print("\t");
                pw.println(records.get(k));
            }
            System.out.println("Merged results written to " + mergedFile.getAbsolutePath());

        } catch (IOException x) {
            x.printStackTrace();
        }


    }
}
