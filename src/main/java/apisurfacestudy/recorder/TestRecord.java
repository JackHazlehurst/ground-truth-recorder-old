package apisurfacestudy.recorder;

import java.util.Objects;

public class TestRecord implements Comparable<TestRecord> {
    public enum TestAnnotation {
        BEFORE_ALL, AFTER_ALL, BEFORE_EACH, AFTER_EACH, TEST, JUNIT_SETUP
    }
    /*
    Used to record methods while junit is being setup, before (or after) tests are called
     */
    public static final TestRecord JUNIT_SETUP = new TestRecord(TestAnnotation.JUNIT_SETUP, null, null, null);

    public final TestAnnotation annotation;
    public final String testClass;
    public final String testMethod;
    public final String value;

    public TestRecord(TestAnnotation annotation, String testClass, String testMethod, String value) {
        this.annotation = annotation;
        this.testClass = testClass;
        this.testMethod = testMethod;
        this.value = value;
    }

    public TestRecord(TestRecord testRecord, TestAnnotation annotation) {
        this.annotation = annotation;
        this.testClass = testRecord == null ? null : testRecord.testClass;
        this.testMethod = testRecord == null ? null : testRecord.testMethod;
        this.value = testRecord == null ? null : testRecord.value;
    }

    public TestRecord(String testMethod, TestRecord testRecord) {
        this.annotation = testRecord == null ? null : testRecord.annotation;
        this.testClass = testRecord == null ? null : testRecord.testClass;
        this.testMethod = testMethod;
        this.value = testRecord == null ? null : testRecord.value;
    }

    public TestRecord(TestRecord testRecord, String value) {
        this.annotation = testRecord == null ? null : testRecord.annotation;
        this.testClass = testRecord == null ? null : testRecord.testClass;
        this.testMethod = testRecord == null ? null : testRecord.testMethod;
        this.value = value;
    }

    @Override
    public int compareTo(TestRecord testRecord) {
        return this.toString().compareTo(testRecord.toString());
    }

    @Override
    public String toString() {
        return "TestRecord{" +
                "testClass='" + testClass + '\'' +
                ", testMethod='" + testMethod + '\'' +
                ", annotation=" + annotation +
                ", value='" + value + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestRecord that = (TestRecord) o;
        return annotation == that.annotation &&
                Objects.equals(testClass, that.testClass) &&
                Objects.equals(testMethod, that.testMethod) &&
                Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(annotation, testClass, testMethod, value);
    }
}
