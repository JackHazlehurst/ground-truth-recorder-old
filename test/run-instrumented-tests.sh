#!/bin/bash

# switch to java 8 (platform specific !!)
export JAVA_HOME=`/usr/libexec/java_home -v 1.8`

echo 'JVM used is ' $JAVA_HOME
echo 'checking Java version (must be 8)'
java -version

echo 'building and extracting dependencies'
mvn clean package dependency:copy-dependencies

echo 'running tests'
java -javaagent:aspectj-1.9.4/lib/aspectjweaver.jar -Daj.weaving.verbose=true -cp api-surface-recorder-1.0.1.jar:target/dependency/*:target/classes:target/test-classes org.junit.runner.JUnitCore test.com.foo.Test

