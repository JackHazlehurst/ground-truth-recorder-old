package com.foo;

import java.util.Objects;

public class Bean {

    private String field = null;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bean bean = (Bean) o;
        return Objects.equals(field, bean.field);
    }

    @Override
    public int hashCode() {
        return Objects.hash(field);
    }
}
